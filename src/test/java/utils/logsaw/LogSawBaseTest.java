package utils.logsaw;

import org.junit.Test;

import java.io.IOException;
import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class LogSawBaseTest {

    public static final String START_SAWING_DATE = "10:00:00";
    public static final String END_SAWING_DATE = "10:30:00";

    public static final String BEFORE_START_SAWING_DATE = "09:00:00 should not be logged\n" +
            "09:00:01 should not be logged\n" +
            "09:10:00 should not be logged";
    public static final String WITHIN_SAWING_DATES = "10:00:00 should be logged\n" +
            "10:01:00 should be logged\n" +
            "10:10:00 should be logged";
    public static final String AFTER_END_SAWING_DATE = "10:30:01 should not be logged\n" +
            "11:00:00 should not be logged\n" +
            "11:10:00 should not be logged";
    public static final String LINE_WITH_NO_DATE = "Line with no date";
    public static final String LINE_WITH_NO_DATE_THAT_SHOULD_BE_LOGGED = "Line with no date. Should be logged.";

    public static final String BEFORE_AND_WITHIN_SAWING_DATES = BEFORE_START_SAWING_DATE + "\n" + WITHIN_SAWING_DATES;
    public static final String WITHIN_AND_AFTER_SAWING_DATES = WITHIN_SAWING_DATES + "\n" + AFTER_END_SAWING_DATE;
    public static final String BEFORE_WITHIN_AND_AFTER_SAWING_DATES = BEFORE_START_SAWING_DATE + "\n" + WITHIN_SAWING_DATES + "\n" + AFTER_END_SAWING_DATE;

    public static final String BEFORE_AND_WITHIN_SAWING_DATES_WITH_UNDATED_LINES =
            LINE_WITH_NO_DATE_THAT_SHOULD_BE_LOGGED
                    + "\n" + BEFORE_START_SAWING_DATE
                    + "\n" + LINE_WITH_NO_DATE
                    + "\n" + WITHIN_SAWING_DATES
                    + "\n" + LINE_WITH_NO_DATE_THAT_SHOULD_BE_LOGGED;
    public static final String WITHIN_AND_AFTER_SAWING_DATES_WITH_UNDATED_LINES =
            LINE_WITH_NO_DATE_THAT_SHOULD_BE_LOGGED
                    + "\n" + WITHIN_SAWING_DATES
                    + "\n" + LINE_WITH_NO_DATE_THAT_SHOULD_BE_LOGGED
                    + "\n" + AFTER_END_SAWING_DATE
                    + "\n" + LINE_WITH_NO_DATE;
    public static final String BEFORE_WITHIN_AND_AFTER_SAWING_DATES_WITH_UNDATED_LINES =
            LINE_WITH_NO_DATE_THAT_SHOULD_BE_LOGGED
                    + "\n" + BEFORE_START_SAWING_DATE
                    + "\n" + LINE_WITH_NO_DATE
                    + "\n" + WITHIN_SAWING_DATES
                    + "\n" + LINE_WITH_NO_DATE_THAT_SHOULD_BE_LOGGED
                    + "\n" + AFTER_END_SAWING_DATE
                    + "\n" + LINE_WITH_NO_DATE;
    public static final String SAWED_RIGHT_RESULT_WITH_UNDATED_LINES =
            LINE_WITH_NO_DATE_THAT_SHOULD_BE_LOGGED
                    + "\n" + WITHIN_SAWING_DATES
                    + "\n" + LINE_WITH_NO_DATE_THAT_SHOULD_BE_LOGGED;


    protected LogSaw logSaw;

    @Test
    public void testSawAllWithinDate() throws IOException {

        String collected = logSaw.saw(getStreamOfLines(WITHIN_SAWING_DATES), START_SAWING_DATE, END_SAWING_DATE).collect(Collectors.joining("\n"));
        assertThat(collected, is(WITHIN_SAWING_DATES));
    }

    @Test
    public void testSawSawingLinesBefore() throws IOException {

        String collected = logSaw.saw(getStreamOfLines(BEFORE_AND_WITHIN_SAWING_DATES), START_SAWING_DATE, END_SAWING_DATE).collect(Collectors.joining("\n"));
        assertThat(collected, is(WITHIN_SAWING_DATES));
    }

    @Test
    public void testSawSawingLinesAfter() throws IOException {

        String collected = logSaw.saw(getStreamOfLines(WITHIN_AND_AFTER_SAWING_DATES), START_SAWING_DATE, END_SAWING_DATE).collect(Collectors.joining("\n"));
        assertThat(collected, is(WITHIN_SAWING_DATES));
    }

    @Test
    public void testSawSawingLinesBeforeAndAfter() throws IOException {

        String collected = logSaw.saw(getStreamOfLines(BEFORE_WITHIN_AND_AFTER_SAWING_DATES), START_SAWING_DATE, END_SAWING_DATE).collect(Collectors.joining("\n"));
        assertThat(collected, is(WITHIN_SAWING_DATES));
    }

    @Test
    public void testSawSawingLinesBeforeWithUndatedLines() throws IOException {

        String collected = logSaw.saw(getStreamOfLines(BEFORE_AND_WITHIN_SAWING_DATES_WITH_UNDATED_LINES), START_SAWING_DATE, END_SAWING_DATE).collect(Collectors.joining("\n"));
        assertThat(collected, is(SAWED_RIGHT_RESULT_WITH_UNDATED_LINES));
    }

    @Test
    public void testSawSawingLinesAfterWithUndatedLines() throws IOException {

        String collected = logSaw.saw(getStreamOfLines(WITHIN_AND_AFTER_SAWING_DATES_WITH_UNDATED_LINES), START_SAWING_DATE, END_SAWING_DATE).collect(Collectors.joining("\n"));
        assertThat(collected, is(SAWED_RIGHT_RESULT_WITH_UNDATED_LINES));
    }

    @Test
    public void testSawSawingLinesBeforeAndAfterWithUndatedLines() throws IOException {

        String collected = logSaw.saw(getStreamOfLines(BEFORE_WITHIN_AND_AFTER_SAWING_DATES_WITH_UNDATED_LINES), START_SAWING_DATE, END_SAWING_DATE).collect(Collectors.joining("\n"));
        assertThat(collected, is(SAWED_RIGHT_RESULT_WITH_UNDATED_LINES));
    }

    protected Stream<String> getStreamOfLines(String lines) {
        return Arrays.stream(lines.split("\n"));
    }

}
