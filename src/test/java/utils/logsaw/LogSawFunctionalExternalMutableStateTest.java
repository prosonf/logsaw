package utils.logsaw;

import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

public class LogSawFunctionalExternalMutableStateTest extends LogSawBaseTest {

    @Before
    public void setup() throws IOException {
        logSaw = new LogSawFunctionalExternalMutableState();
    }

    @Test
    public void testMapToLineState() {

        List<LogSawFunctionalExternalMutableState.LineState> lineStates = getStreamOfLines(WITHIN_AND_AFTER_SAWING_DATES_WITH_UNDATED_LINES).map(LogSaw::toLineState).collect(Collectors.toList());

        assertThat(lineStates, is(notNullValue()));
        assertThat(lineStates.size(), is(9));
        lineStates.forEach(s -> {
            assertThat(s, is(notNullValue()));
            if (s.line.startsWith("Line")) {
                assertThat(s.actualTimestamp, is(Optional.empty()));
                assertThat(s.effectiveTimestamp, is(""));
            } else {
                assertThat(s.actualTimestamp.get().length(), is(8));
                assertThat(s.effectiveTimestamp, is(""));
            }
        });
    }
}
