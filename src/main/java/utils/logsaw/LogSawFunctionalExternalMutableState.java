package utils.logsaw;

import java.io.IOException;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class LogSawFunctionalExternalMutableState implements LogSaw {

    public static void main(String[] args) throws IOException {
        LogSaw.validateAndLaunch(new LogSawFunctionalExternalMutableState(), args);
    }

    @Override
    public Stream<String> saw(Stream<String> lines, String startTime, String endTime) throws IOException {

        Predicate<LineState> isMeantForWriting = LogSaw.isBetweenTimeInLinePredicate(startTime, endTime);

        LineState previousLine = LineState.newEmpty(); // Mutable state
        return lines.map(LogSaw::toLineState).map(lineState -> {
            String effectiveTimestamp = lineState.actualTimestamp.orElse(previousLine.effectiveTimestamp);
            previousLine.effectiveTimestamp = effectiveTimestamp;
            return new LineState(lineState.line, lineState.actualTimestamp, effectiveTimestamp);
        }).filter(isMeantForWriting).map(ls -> ls.line);
    }
}
