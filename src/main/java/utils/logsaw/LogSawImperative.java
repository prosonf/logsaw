package utils.logsaw;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class LogSawImperative implements LogSaw {

    public static void main(String[] args) throws IOException {
        LogSaw.validateAndLaunch(new LogSawImperative(), args);
    }

    @Override
    public Stream<String> saw(Stream<String> lines, String startTime, String endTime) throws IOException {

        Predicate<String> isMeantForWriting = LogSaw.isBetweenTimeInLinePredicateForString(startTime, endTime);
        List<String> linesList = lines.collect(Collectors.toList());
        List<String> nonFilteredLines = new ArrayList<>();
        Predicate<String> isDated = isDatedPredicate();
        Boolean wasLastOneFiltered = Boolean.TRUE;
        for (String line: linesList) {
            Boolean filterThisOne = isDated.test(line) ? isMeantForWriting.test(line) : wasLastOneFiltered;
            wasLastOneFiltered = filterThisOne;
            if (filterThisOne) {
                nonFilteredLines.add(line);
            }
        }

        return nonFilteredLines.stream();
    }

    protected Predicate<String> isDatedPredicate() throws IOException {
        return s -> HOUR.matcher(s).matches();
    }
}
