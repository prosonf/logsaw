package utils.logsaw;

import java.io.IOException;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class LogSawFunctionalInternalMutableState implements LogSaw {

    public static void main(String[] args) throws IOException {
        LogSaw.validateAndLaunch(new LogSawFunctionalInternalMutableState(), args);
    }

    @Override
    public Stream<String> saw(Stream<String> lines, String startTime, String endTime) throws IOException {

        Predicate<LineState> isMeantForWriting = LogSaw.isBetweenTimeInLinePredicate(startTime, endTime);
        return lines.map(LogSaw::toLineState).map(State.newState()).filter(isMeantForWriting).map(ls -> ls.line);
    }

    public static class State implements Function<LineState, LineState> {
        private Optional<String> lastTimestamp = Optional.of(LineState.DEFAULT_EFFECTIVE_TS);

        @Override
        public LineState apply(LineState lineState) {

            LineState newLineState = new LineState(lineState.line, lineState.actualTimestamp);
            lastTimestamp.flatMap(newLineState::setEffectiveTimestamp);
            lineState.actualTimestamp.flatMap(newLineState::setEffectiveTimestamp);
            lastTimestamp = Optional.of(newLineState.effectiveTimestamp);
            return newLineState;
        }
        public static State newState() {
            return new State();
        }
    }
}

