package utils.logsaw;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

public interface LogSaw {

    Pattern HOUR = Pattern.compile(".*([0-9]{2}:[0-9]{2}:[0-9]{2}).*");

    static void validateAndLaunch(LogSaw lowSawObject, String[] args) throws IOException {
        if (args.length != 3) {

            System.out.println(String.format("Usage: java %s path startTime endTime", lowSawObject.getClass().getCanonicalName()));
            System.exit(1);
        }
        lowSawObject.saw(args[0], args[1], args[2]);
    }

    default void saw(String pathString, String startTime, String endTime) throws IOException {

        saw(Paths.get(pathString), startTime, endTime);
    }

    default void saw(Path path, String startTime, String endTime) throws IOException {

        if (Files.isDirectory(path)) {
            Files.list(path).forEach(filePath -> {
                try {
                    saw(filePath, startTime, endTime);
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            });
        } else {
            sawFile(path, startTime, endTime);
        }
    }

    default void sawFile(Path path, String startTime, String endTime) {

        Path pathForWriting = Paths.get(path.getFileName() + ".sawed");
        try (Stream<String> lines = Files.lines(path); BufferedWriter writer = Files.newBufferedWriter(pathForWriting)) {

            Consumer<String> write = lineToFile(writer);

            saw(lines, startTime, endTime).forEach(write);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    Stream<String> saw(Stream<String> lines, String startTime, String endTime) throws IOException;

    default Consumer<String> lineToFile(BufferedWriter writer) {
        return l -> {
            try {
                writer.write(l + "\n");
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        };
    }

    static LineState toLineState(String line) {

        return new LineState(line, extractDate(line));
    }

    static Optional<String> extractDate(String s) {
        Matcher matcher = HOUR.matcher(s);
        return matcher.matches() ? Optional.ofNullable(matcher.group(1)) : Optional.<String>empty();
    }

    class LineState {
        public static final String DEFAULT_EFFECTIVE_TS = "";
        String line;
        Optional<String> actualTimestamp;
        String effectiveTimestamp;

        public LineState() {
            this.effectiveTimestamp = DEFAULT_EFFECTIVE_TS;
        }

        public LineState(String line) {
            this();
            this.line = line;
        }

        public LineState(String line, Optional<String> actualTimestamp) {
            this(line);
            this.actualTimestamp = actualTimestamp;
        }

        public LineState(String line, Optional<String> actualTimestamp, String effectiveTimestamp) {
            this(line, actualTimestamp);
            this.setEffectiveTimestamp(effectiveTimestamp);
        }

        public Optional<String> setEffectiveTimestamp(String effectiveTimestamp) {
            this.effectiveTimestamp = effectiveTimestamp;
            return Optional.ofNullable(effectiveTimestamp);
        }

        public static LineState newEmpty() {
            return new LineState();
        }
    }

    static Predicate<LineState> isBetweenTimeInLinePredicate(String startTime, String endTime) throws IOException {
        Predicate<String> betweenTimeInLinePredicateForString = isBetweenTimeInLinePredicateForString(startTime, endTime);
        return ls -> ls.effectiveTimestamp.isEmpty() || betweenTimeInLinePredicateForString.test(ls.effectiveTimestamp);
    }

    static Predicate<String> isBetweenTimeInLinePredicateForString(String startTime, String endTime) throws IOException {
        return isBetweenTimeAnywhereInLinePredicate(startTime, endTime);
    }

    static Predicate<String> isBetweenTimeAnywhereInLinePredicate(String startTime, String endTime) throws IOException {
        return s -> {
            String group = null;
            Matcher matcher = HOUR.matcher(s);
            if (matcher.matches()) {
                group = matcher.group(1);
            }
            return group != null && isBetweenTimePredicate(startTime, endTime).test(group);
        };
    }

    static Predicate<String> isBetweenTimePredicate(String startTime, String endTime) {
        return s -> s.compareTo(startTime) >= 0 && s.compareTo(endTime) <= 0;
    }
}
